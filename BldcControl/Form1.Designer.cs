﻿namespace BldcControl
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.trackBarPWM = new System.Windows.Forms.TrackBar();
            this.checkBoxPWM = new System.Windows.Forms.CheckBox();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonCom = new System.Windows.Forms.Button();
            this.labelPWM = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.labelMaxCurrent = new System.Windows.Forms.Label();
            this.labelCurrent = new System.Windows.Forms.Label();
            this.labelOverCurrent = new System.Windows.Forms.Label();
            this.labelOverLoad = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelSpeed = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPWM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBarPWM
            // 
            this.trackBarPWM.AutoSize = false;
            this.trackBarPWM.Cursor = System.Windows.Forms.Cursors.Default;
            this.trackBarPWM.Location = new System.Drawing.Point(981, 67);
            this.trackBarPWM.Maximum = 100;
            this.trackBarPWM.Name = "trackBarPWM";
            this.trackBarPWM.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarPWM.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.trackBarPWM.Size = new System.Drawing.Size(45, 163);
            this.trackBarPWM.TabIndex = 0;
            this.trackBarPWM.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.trackBarPWM.Scroll += new System.EventHandler(this.trackBarPWM_Scroll);
            // 
            // checkBoxPWM
            // 
            this.checkBoxPWM.AutoSize = true;
            this.checkBoxPWM.Location = new System.Drawing.Point(981, 237);
            this.checkBoxPWM.Name = "checkBoxPWM";
            this.checkBoxPWM.Size = new System.Drawing.Size(48, 16);
            this.checkBoxPWM.TabIndex = 1;
            this.checkBoxPWM.Text = "反向";
            this.checkBoxPWM.UseVisualStyleBackColor = true;
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(12, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(94, 20);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.Click += new System.EventHandler(this.comboBox1_Click);
            // 
            // buttonCom
            // 
            this.buttonCom.Location = new System.Drawing.Point(112, 10);
            this.buttonCom.Name = "buttonCom";
            this.buttonCom.Size = new System.Drawing.Size(75, 23);
            this.buttonCom.TabIndex = 3;
            this.buttonCom.Text = "连接";
            this.buttonCom.UseVisualStyleBackColor = true;
            this.buttonCom.Click += new System.EventHandler(this.buttonCom_Click);
            // 
            // labelPWM
            // 
            this.labelPWM.AutoSize = true;
            this.labelPWM.Location = new System.Drawing.Point(988, 53);
            this.labelPWM.Name = "labelPWM";
            this.labelPWM.Size = new System.Drawing.Size(11, 12);
            this.labelPWM.TabIndex = 4;
            this.labelPWM.Text = "0";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(9, 469);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(497, 163);
            this.textBox1.TabIndex = 5;
            // 
            // labelMaxCurrent
            // 
            this.labelMaxCurrent.AutoSize = true;
            this.labelMaxCurrent.Location = new System.Drawing.Point(853, 53);
            this.labelMaxCurrent.Name = "labelMaxCurrent";
            this.labelMaxCurrent.Size = new System.Drawing.Size(71, 12);
            this.labelMaxCurrent.TabIndex = 6;
            this.labelMaxCurrent.Text = "Max Current";
            // 
            // labelCurrent
            // 
            this.labelCurrent.AutoSize = true;
            this.labelCurrent.Location = new System.Drawing.Point(853, 67);
            this.labelCurrent.Name = "labelCurrent";
            this.labelCurrent.Size = new System.Drawing.Size(47, 12);
            this.labelCurrent.TabIndex = 7;
            this.labelCurrent.Text = "Current";
            // 
            // labelOverCurrent
            // 
            this.labelOverCurrent.AutoSize = true;
            this.labelOverCurrent.Location = new System.Drawing.Point(853, 89);
            this.labelOverCurrent.Name = "labelOverCurrent";
            this.labelOverCurrent.Size = new System.Drawing.Size(83, 12);
            this.labelOverCurrent.TabIndex = 8;
            this.labelOverCurrent.Text = "OverCurrent:0";
            // 
            // labelOverLoad
            // 
            this.labelOverLoad.AutoSize = true;
            this.labelOverLoad.Location = new System.Drawing.Point(853, 113);
            this.labelOverLoad.Name = "labelOverLoad";
            this.labelOverLoad.Size = new System.Drawing.Size(65, 12);
            this.labelOverLoad.TabIndex = 9;
            this.labelOverLoad.Text = "OverLoad:0";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox1.Location = new System.Drawing.Point(12, 47);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(800, 400);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // labelSpeed
            // 
            this.labelSpeed.AutoSize = true;
            this.labelSpeed.Location = new System.Drawing.Point(853, 143);
            this.labelSpeed.Name = "labelSpeed";
            this.labelSpeed.Size = new System.Drawing.Size(41, 12);
            this.labelSpeed.TabIndex = 11;
            this.labelSpeed.Text = "Speed:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1067, 657);
            this.Controls.Add(this.labelSpeed);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.labelOverLoad);
            this.Controls.Add(this.labelOverCurrent);
            this.Controls.Add(this.labelCurrent);
            this.Controls.Add(this.labelMaxCurrent);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.labelPWM);
            this.Controls.Add(this.buttonCom);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.checkBoxPWM);
            this.Controls.Add(this.trackBarPWM);
            this.Name = "Form1";
            this.Text = "BLDC Control";
            ((System.ComponentModel.ISupportInitialize)(this.trackBarPWM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBarPWM;
        private System.Windows.Forms.CheckBox checkBoxPWM;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonCom;
        private System.Windows.Forms.Label labelPWM;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label labelMaxCurrent;
        private System.Windows.Forms.Label labelCurrent;
        private System.Windows.Forms.Label labelOverCurrent;
        private System.Windows.Forms.Label labelOverLoad;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label labelSpeed;
    }
}

