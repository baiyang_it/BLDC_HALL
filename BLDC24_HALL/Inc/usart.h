/**
  ******************************************************************************
  * @file    usart.h
  * @brief   This file contains all the function prototypes for
  *          the usart.c file
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __USART_H__
#define __USART_H__

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

enum
{
    CMD_PWM = 1,
    CMD_STATE,
};

typedef struct UPDATE_STATE
{
	uint16_t vbus;
	uint16_t current;
	uint16_t speed;
	uint8_t over_current;
	uint8_t over_load;
	uint8_t hall_step[6];
} UPDATE_STATE_T;

extern uint8_t uart_ctrl_flag;
extern uint16_t uart_ctrl_pwm;
extern uint8_t uart_ctrl_dir;

extern UART_HandleTypeDef huart2;


void MX_USART2_UART_Init(uint32_t baudrate);

void uart_timer_callback(void);
void uart2_send_byte(uint8_t data);
void uart2_send_buf(uint8_t * buf, int len);
void cmd_state_send(UPDATE_STATE_T state);

#ifdef __cplusplus
}
#endif

#endif /* __USART_H__ */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
