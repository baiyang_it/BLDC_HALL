#ifndef __BLDC_H__
#define __BLDC_H__

#include "main.h"

#define DIR_CW 0
#define DIR_CCW 1

extern uint8_t step_table[6];
extern uint8_t hall;
extern uint8_t last_hall;
extern uint8_t direction;
extern uint8_t current_step;

void bldc_init(void);
void output_ahcl(uint16_t pwm);
void output_bhcl(uint16_t pwm);
void output_bhal(uint16_t pwm);
void output_chal(uint16_t pwm);
void output_chbl(uint16_t pwm);
void output_ahbl(uint16_t pwm);
void output_zero(void);
void output_brake(void);

void output_step_1(uint16_t pwm);
void output_step_2(uint16_t pwm);
void output_step_3(uint16_t pwm);
void output_step_4(uint16_t pwm);
void output_step_5(uint16_t pwm);
void output_step_6(uint16_t pwm);

void force_run(void);
uint8_t hall_get_position(void);
void calc_step_table(void);
void bldc_convert(uint16_t pwm);
void bldc_timer_callback(void);
void bldc_hall_change_callback(void);

#endif

