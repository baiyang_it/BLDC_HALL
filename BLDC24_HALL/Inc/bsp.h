#ifndef __BSP_H__
#define __BSP_H__

#include "main.h"
#include "gpio.h"
#include "usart.h"
#include "can.h"
#include "tim.h"
#include "adc.h"


void SystemClock_Config(void);
void MX_IWDG_Init(void);
void MX_IWDG_Refresh(void);

#endif


