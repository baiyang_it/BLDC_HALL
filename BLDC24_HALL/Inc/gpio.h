/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#define LED_PORT GPIOB
#define LED_PIN	 GPIO_PIN_12

#define HALL1_PORT	GPIOB
#define HALL1_PIN	GPIO_PIN_3
#define HALL2_PORT	GPIOB
#define HALL2_PIN	GPIO_PIN_4
#define HALL3_PORT	GPIOB
#define HALL3_PIN	GPIO_PIN_5

#define HOUT_1_PORT	GPIOB
#define HOUT_1_PIN  GPIO_PIN_1
#define HOUT_2_PORT	GPIOB
#define HOUT_2_PIN  GPIO_PIN_0
#define HOUT_3_PORT	GPIOA
#define HOUT_3_PIN  GPIO_PIN_7

#define LOUT_1_PORT	GPIOA
#define LOUT_1_PIN  GPIO_PIN_8
#define LOUT_2_PORT	GPIOA
#define LOUT_2_PIN  GPIO_PIN_9
#define LOUT_3_PORT	GPIOA
#define LOUT_3_PIN  GPIO_PIN_10

#define PWM_PORT	GPIOB
#define PWM_PIN		GPIO_PIN_6

#define IOVER_PORT	GPIOB
#define IOVER_PIN	GPIO_PIN_2

#define SPEED_PORT	GPIOB
#define SPEED_PIN	GPIO_PIN_9

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */

/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
