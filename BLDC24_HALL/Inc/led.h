#ifndef __LED_H__
#define __LED_H__

#include "main.h"

void led_on(void);
void led_off(void);
void led_output(uint32_t ms);
void led_pool(void);


#endif
