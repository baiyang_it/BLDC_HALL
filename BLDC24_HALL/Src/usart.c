/**
  ******************************************************************************
  * @file    usart.c
  * @brief   This file provides code for the configuration
  *          of the USART instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

UART_HandleTypeDef huart2;

uint8_t uart2_rx_data;
uint8_t uart2_rx_state=0;
uint8_t uart2_rx_count = 0;
uint8_t uart2_rx_buffer[100];

uint8_t uart_package_len = 0;

uint8_t uart_ctrl_flag = 0;
uint16_t uart_ctrl_pwm = 0;
uint8_t uart_ctrl_dir = 0;

#define UART2_TX_BUF_SIZE   100
uint8_t uart2_tx_buf[UART2_TX_BUF_SIZE];
uint8_t uart2_tx_count = 0;
uint8_t uart2_tx_write = 0;
uint8_t uart2_tx_read = 0;

/* USART2 init function */

void MX_USART2_UART_Init(uint32_t baudrate)
{
    huart2.Instance = USART2;
    huart2.Init.BaudRate = baudrate;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart2) != HAL_OK)
    {
        Error_Handler();
    }

    //HAL_UART_Receive_IT(&huart2,&uart2_rx_data,1);
    __HAL_UART_ENABLE_IT(&huart2, UART_IT_RXNE);
}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspInit 0 */

  /* USER CODE END USART2_MspInit 0 */
    /* USART2 clock enable */
    __HAL_RCC_USART2_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART2 interrupt Init */
    HAL_NVIC_SetPriority(USART2_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspInit 1 */

  /* USER CODE END USART2_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==USART2)
  {
  /* USER CODE BEGIN USART2_MspDeInit 0 */

  /* USER CODE END USART2_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART2_CLK_DISABLE();

    /**USART2 GPIO Configuration
    PA2     ------> USART2_TX
    PA3     ------> USART2_RX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2|GPIO_PIN_3);

    /* USART2 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART2_IRQn);
  /* USER CODE BEGIN USART2_MspDeInit 1 */

  /* USER CODE END USART2_MspDeInit 1 */
  }
}

uint8_t calc_bcc(uint8_t * buf, int len)
{
    uint8_t bcc = 0;
    for(int i=0;i<len;i++)
    {
        bcc ^= buf[i];
    }

    return bcc;
}

void cmd_pwm_process(uint8_t * buf)
{
    uart_ctrl_dir = buf[3];
    uart_ctrl_pwm = buf[4];
    uart_ctrl_flag = 1;
}

void uart_package_parse(uint8_t * buf)
{
    uint8_t cmd = buf[2];
    switch(cmd)
    {
        case CMD_PWM:
            cmd_pwm_process(buf);
            break;
    }
}

void uart2_rx_irq(uint8_t data)
{    
    uint8_t bcc;
    
    switch(uart2_rx_state)
    {
        case 0: //recv STX
            if(data == 0x02)
            {
                uart2_rx_state = 1;
                uart2_rx_buffer[0] = data;
                uart2_rx_count = 1;
            }
            break;
        case 1: //recv len
            uart_package_len = data;
            if(uart_package_len < 4 || uart_package_len > sizeof(uart2_rx_buffer)) //package len error
            {
                uart2_rx_state = 0;
                uart2_rx_count = 0;
                break;
            }
            
            uart2_rx_buffer[uart2_rx_count] = data;
            uart2_rx_count++;
            uart2_rx_state = 2;
            break;
        case 2: //recv data
            uart2_rx_buffer[uart2_rx_count] = data;
            uart2_rx_count++;
        
            if(uart2_rx_count == uart_package_len)
            {
                bcc = calc_bcc(uart2_rx_buffer, uart_package_len - 1);
                if(bcc == uart2_rx_buffer[uart_package_len - 1])
                {
                    /* recv packet success */
                    uart_package_parse(uart2_rx_buffer);
                }
                
                uart2_rx_state = 0;
                uart2_rx_count = 0;
                memset(uart2_rx_buffer, 0 ,sizeof(uart2_rx_buffer));
            }
            break;
    }
}

void uart2_txe_irq(void)
{
    if(uart2_tx_count > 0)
    {
        USART2->DR = uart2_tx_buf[uart2_tx_read];
        uart2_tx_read++;
        if(uart2_tx_read == UART2_TX_BUF_SIZE)
        {
            uart2_tx_read = 0;
        }
        uart2_tx_count--;
    }
    else
    {
        __HAL_UART_DISABLE_IT(&huart2, UART_IT_TXE);
    }
}

void uart_timer_callback(void)
{
        
}

/**
  * @brief This function handles USART2 global interrupt.
  */
void USART2_IRQHandler(void)
{
    uint8_t data;
    
    if(__HAL_UART_GET_FLAG(&huart2, UART_FLAG_RXNE))
    {
        data = USART2->DR;
        uart2_rx_irq(data);
        
        __HAL_UART_CLEAR_FLAG(&huart2, UART_FLAG_RXNE);
    }
    
    if(__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TXE))
    {
        uart2_txe_irq();
        
        __HAL_UART_CLEAR_FLAG(&huart2, UART_FLAG_TXE);
    }
}

void uart2_send_byte(uint8_t data)
{
    __HAL_UART_DISABLE_IT(&huart2, UART_IT_TXE);
    
    if(uart2_tx_count < UART2_TX_BUF_SIZE)
    {
        uart2_tx_buf[uart2_tx_write] = data;
        uart2_tx_count++;
        uart2_tx_write++;
        if(uart2_tx_write == UART2_TX_BUF_SIZE)
        {
            uart2_tx_write = 0;
        }
        
        __HAL_UART_ENABLE_IT(&huart2, UART_IT_TXE);
    }    
}

void uart2_send_buf(uint8_t * buf, int len)
{
    int i;
    for(i=0;i<len;i++)
    {
        uart2_send_byte(buf[i]);
    }
}

void cmd_state_send(UPDATE_STATE_T state)
{
	uint8_t buf[100];

	buf[0] = 0x02;
	buf[1] = 12;
	buf[2] = CMD_STATE;
	buf[3] = state.vbus >>8;
	buf[4] = state.vbus & 0xFF;	
	buf[5] = state.current >>8;
	buf[6] = state.current & 0xFF;
	buf[7] = state.speed >>8;
	buf[8] = state.speed & 0xFF;
	buf[9] = state.over_current;
	buf[10] = state.over_load;
	buf[11] = calc_bcc(buf,11);
	uart2_send_buf(buf,12);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
