#include "led.h"
#include "gpio.h"

uint32_t ledDelay=0;

void led_on(void)
{
	HAL_GPIO_WritePin(LED_PORT,LED_PIN,GPIO_PIN_RESET);
}

void led_off(void)
{
	HAL_GPIO_WritePin(LED_PORT,LED_PIN,GPIO_PIN_SET);
}

void led_output(uint32_t ms)
{
	ledDelay = ms;
	led_on();
}

void led_pool(void)
{
	if(ledDelay>0)
	{
		ledDelay--;
		
	}
	else
	{
		//led_off();
	}
}


