/**
  ******************************************************************************
  * File Name          : gpio.c
  * Description        : This file provides code for the configuration
  *                      of all used GPIO pins.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "gpio.h"
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/*----------------------------------------------------------------------------*/
/* Configure GPIO                                                             */
/*----------------------------------------------------------------------------*/
/* USER CODE BEGIN 1 */

/* USER CODE END 1 */

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
void MX_GPIO_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	/* GPIO Ports Clock Enable */
	__HAL_RCC_GPIOA_CLK_ENABLE();
	__HAL_RCC_GPIOB_CLK_ENABLE();

	/* Configure LED gpio */
	HAL_GPIO_WritePin(LED_PORT, LED_PIN, GPIO_PIN_RESET);
	
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pin = LED_PIN;
	HAL_GPIO_Init(LED_PORT, &GPIO_InitStruct);
	
	/* Configure speed gpio */
	HAL_GPIO_WritePin(SPEED_PORT, SPEED_PIN, GPIO_PIN_RESET);
	
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.Pin = SPEED_PIN;
	HAL_GPIO_Init(SPEED_PORT, &GPIO_InitStruct);


	/* Configure Hall gpio */
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Pin = HALL1_PIN;
	HAL_GPIO_Init(HALL1_PORT, &GPIO_InitStruct);
	GPIO_InitStruct.Pin = HALL2_PIN;
	HAL_GPIO_Init(HALL2_PORT, &GPIO_InitStruct);
	GPIO_InitStruct.Pin = HALL3_PIN;
	HAL_GPIO_Init(HALL3_PORT, &GPIO_InitStruct);

    /* Configure Low Out gpio */
    HAL_GPIO_WritePin(LOUT_1_PORT, LOUT_1_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LOUT_2_PORT, LOUT_2_PIN, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(LOUT_3_PORT, LOUT_3_PIN, GPIO_PIN_RESET);

    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
    GPIO_InitStruct.Pin = LOUT_1_PIN;
    HAL_GPIO_Init(LOUT_1_PORT, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = LOUT_2_PIN;
    HAL_GPIO_Init(LOUT_2_PORT, &GPIO_InitStruct);
    GPIO_InitStruct.Pin = LOUT_3_PIN;
    HAL_GPIO_Init(LOUT_3_PORT, &GPIO_InitStruct);

	/* Configure Current OVER gpio */
	// GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
	// GPIO_InitStruct.Pull = GPIO_NOPULL;
	// GPIO_InitStruct.Pin = IOVER_PIN;
	// HAL_GPIO_Init(IOVER_PORT, &GPIO_InitStruct);

	/* Configure PWM Input gpio */
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Pin = PWM_PIN;
	HAL_GPIO_Init(PWM_PORT, &GPIO_InitStruct);

	/* EXTI interrupt init*/
	// HAL_NVIC_SetPriority(EXTI2_IRQn, 0, 0);
	// HAL_NVIC_EnableIRQ(EXTI2_IRQn);

	HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI3_IRQn);

	HAL_NVIC_SetPriority(EXTI4_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI4_IRQn);

	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

}

/* USER CODE BEGIN 2 */

/* USER CODE END 2 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
